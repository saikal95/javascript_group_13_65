import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {Movie} from "./shared/movie.model";
import {HttpClient} from "@angular/common/http";
import {movieService} from "./shared/movie.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // movies!: Movie[];
  // loading = false;
  // moviesChangeSubscription! : Subscription;
  // moviesFetchingSubscription! : Subscription;
  //
  //
  // constructor(private movieService: movieService,
  //             private http: HttpClient) {}
  //
  // ngOnInit() {
  //   this.loading = true;
  //   this.movies = this.movieService.getMovies();
  //   this.moviesChangeSubscription = this.movieService.moviesChange.subscribe((movies:Movie[]) => {
  //     this.movies = movies;
  //   })
  //   this.moviesFetchingSubscription =  this.movieService.moviesFetching.subscribe((isFetching:boolean) => {
  //     this.loading = isFetching;
  //   })
  //   this.movieService.fetchMovies();
  // }
  //
  // ngOnDestroy() {
  //   this.moviesChangeSubscription.unsubscribe();
  //   this.moviesFetchingSubscription.unsubscribe();
  // }

}
