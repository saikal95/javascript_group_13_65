import {Component, Input, OnInit} from '@angular/core';
import {Movie} from "../shared/movie.model";
import {movieService} from "../shared/movie.service";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Params} from "@angular/router";
import {map} from "rxjs/operators";

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements  OnInit{
   id!: string;
   name! : string;
  @Input() movie!: Movie;
  constructor(private movieService: movieService,   public http: HttpClient, private route: ActivatedRoute,) {}


  ngOnInit(): void {

      // this.http.get<Movie>(`https://another-plovo-default-rtdb.firebaseio.com/movies/${this.id}.json`)
      //   .pipe(map(result => {
      //     return new Movie(
      //       result.id,
      //       result.name,
      //     );
      //   }))
      //   .subscribe(movie => {
      //     this.movie = movie;
      //     this.name= movie.name;
      //     this.id = movie.id;
      //   });

}


   deleteFilm(){
    this.movieService.onDeleteMovie(this.movie.id)
     this.movieService.fetchMovies();
   }



}
