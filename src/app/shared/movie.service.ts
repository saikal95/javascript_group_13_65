import {EventEmitter, Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {Movie} from "./movie.model";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";


@Injectable()
export class movieService {
  moviesChange = new Subject <Movie[]>();
  moviesFetching = new Subject<boolean>();
  name! : string;
  id!: string;

  public movies: any[] = [];

  constructor(
    private http: HttpClient) {

  }

  getMovies() {
    return this.movies.slice();
  }

  fetchMovies(){
    this.moviesFetching.next(true);
    this.http.get<{[id:string]:Movie}>('https://another-plovo-default-rtdb.firebaseio.com/movies.json')
      .pipe(map(result => {
        return Object.keys(result).map(id=> {
          const movieData = result[id];
          this.id = id;
          return new Movie(
            id,
            movieData.name,

          );
        });
      }))
      .subscribe(movies=>{
        this.movies = movies;
        this.moviesChange.next(this.movies.slice());
        this.moviesFetching.next(false);
      })
  }


  sendMovie(name: string) {
    const body = { name};
    console.log(body);
    this.http.post('https://another-plovo-default-rtdb.firebaseio.com/movies.json', body).subscribe();
    this.fetchMovies();

}


   onDeleteMovie(id: string) {
       this.http.delete<Movie>(`https://another-plovo-default-rtdb.firebaseio.com/movies/${this.id}.json`)
         .subscribe();
     this.fetchMovies();
   }



  addMovie(movie: string) {
    this.movies.push(movie);
    this.moviesChange.next(this.movies.slice());
  }
}
