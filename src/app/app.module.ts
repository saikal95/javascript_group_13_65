import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import {movieService} from "./shared/movie.service";
import {HttpClientModule} from "@angular/common/http";
import { AddMoviesComponent } from './add-movies/add-movies.component';
import { MovieComponent } from './movie/movie.component';
import { MoviesComponent } from './movies/movies.component';

@NgModule({
  declarations: [
    AppComponent,
    AddMoviesComponent,
    MovieComponent,
    MoviesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [movieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
