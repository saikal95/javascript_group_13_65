import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Subscription} from "rxjs";
import {movieService} from "../shared/movie.service";
import {Movie} from "../shared/movie.model";

@Component({
  selector: 'app-add-movies',
  templateUrl: './add-movies.component.html',
  styleUrls: ['./add-movies.component.css']
})
export class AddMoviesComponent implements OnInit, OnDestroy{
  @ViewChild('nameInput') nameInput!: ElementRef;

  name!: string;
  loading = false;
  movies!: Movie[];
  moviesChangeSubscription!: Subscription;
  moviesFetchingSubscription!: Subscription;

  constructor(private movieService: movieService) {
  }

  ngOnInit() {
    this.movies = this.movieService.getMovies();
    this.moviesChangeSubscription = this.movieService.moviesChange.subscribe((movies: Movie[]) => {
      this.movies = movies;
    })
    this.moviesFetchingSubscription = this.movieService.moviesFetching.subscribe((isFetching: boolean) => {
      this.loading = isFetching;
    })

  }

  sendMovieName() {
    const name: string = this.nameInput.nativeElement.value;
    this.movieService.sendMovie(name);
    this.movieService.fetchMovies();
  }

  ngOnDestroy() {
    this.moviesChangeSubscription.unsubscribe();
    this.moviesFetchingSubscription.unsubscribe();
  }


}
