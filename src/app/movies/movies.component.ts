import {Component, OnDestroy, OnInit} from '@angular/core';
import {Movie} from "../shared/movie.model";
import {Subscription} from "rxjs";
import {movieService} from "../shared/movie.service";

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit, OnDestroy {
  loading = false;
  movies!: Movie[];
  name!: string;
  moviesChangeSubscription!: Subscription;
  moviesFetchingSubscription!: Subscription;

  constructor(private movieService: movieService) {
  }

  ngOnInit() {
    this.movies = this.movieService.getMovies();
    this.moviesChangeSubscription = this.movieService.moviesChange.subscribe((movies: Movie[]) => {
      this.movies = movies;
    })
    this.moviesFetchingSubscription = this.movieService.moviesFetching.subscribe((isFetching: boolean) => {
      this.loading = isFetching;
    })
    this.movieService.fetchMovies();
  }

  ngOnDestroy() {
    this.moviesChangeSubscription.unsubscribe();
    this.moviesFetchingSubscription.unsubscribe();
  }



}
